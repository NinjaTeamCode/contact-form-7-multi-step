# Contact-Form-7-Multi-Step (Lite version)

We will update the code soon. Please follow us to get news about free WordPress plugin :)

Are you using contact Form 7? This is a nice plugin for you!

Contact Form 7 Multi-Step help you can add multi-step for your form. This is the best way if you form is long and want to display simple for users.

**Live Demo:** [https://ninjateam.org/contact-form-7-multi-step](https://ninjateam.org/contact-form-7-multi-step)

![NinjaTeam Support Center.png](https://bitbucket.org/repo/oGoBXq/images/2948556403-NinjaTeam%20Support%20Center.png)

### FEATURED ###

* Make your form look better with multi-step
* Easy to add step tag to your form with a click
* Easy custom your button for each step
* Unlimited Steps
* Enjoy with nice UI/UX
* Free Support 6 months
* Easy to use
* And more...

### CHANGELOG ###

```
#!python


31/10/2016: (version 1.9.5)
- Fixed: Fix js bug with some hosts.

20/09/2016: (version 1.9.4)
- Added: Some hooks for developers, fix changing button's background color.

14/09/2016: (version 1.9.3)
- Fixed: Fix bug with multi forms

06/08/2016: (version 1.9.2)
- Fixed: Fix small bug

16/07/2016: (version 1.9.1)
- Fixed: Conflict with Visual Composer

07/04/2016: (version 1.9)
- Added: Press Enter to submit

01/04/2016: (version 1.7)
- Added: Loading animation
- Fixed: Auto p
- Fixed: Validation with select
- Fixed: Acceptance tags

28/03/2016: (version 1.6)
- Fixed: Validation

24/03/2016: (version 1.5)
- Added: POT file for translate

24/03/2016: (version 1.4)
- Added: Language translate

23/03/2016: (version 1.3)
- Fixed: CSS

23/03/2016: (version 1.2)
- Fixed: Translate default text in CF7 Message tab

21/03/2016: (version 1.1)
- Fixed: JS conflict

07/03/2016: (version 1.0)
- Version 1.0 Initial Release
```